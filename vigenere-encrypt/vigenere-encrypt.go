package main

import ("fmt"
	"os"
	"io/ioutil"
	"regexp"
	"bytes"
	"strings"
)

func show_usage() {
	fmt.Printf("\033[92m[*]\033[0m Vigenere encryption tool \033[92m[*]\033[0m\n")
	fmt.Printf("A tool for encrypting plaintext files using the vigenere cipher\n\n")
	fmt.Printf("usage: vigenere-encrypt <KEY> <FILE>\n")
}

func check(e error) {
	if e != nil {
		show_usage()
		panic(e)
	}
}

func strip(data string) string {

	re, err := regexp.Compile("[^A-Za-z]+")
	check(err)

	processed_data := re.ReplaceAllString(data, "")

	return processed_data
}

func vigenere_encrypt(key string, data []byte) string {

	var buf bytes.Buffer

	processed_data := strip(string(data))
	var ct = make([]int, len(processed_data))
	var pt = make([]int, len(processed_data))

	var key_len int
	key_len = len(key)

	for i:=0; i<len(processed_data); i++ {
		pt[i] = int(processed_data[i])
		
		ct[i] = pt[i] + (int(key[i%key_len]) - 65)

		if pt[i] >= 65 && pt[i] <= 90 {
			if ct[i] > 90 {
				ct[i] = ct[i] - 26
			}
		} else if pt[i] >= 97 && pt[i] <= 122 {
			if ct[i] > 122 {
				ct[i] = ct[i] - 26
			}
		}
		buf.WriteString(string(ct[i]))

	}

	return strings.ToUpper(buf.String())
}

func main() {
	
	if len(os.Args) < 3 {
		show_usage()
		os.Exit(1)
	}
	
	key := strings.ToUpper(os.Args[1])
	data, err := ioutil.ReadFile(os.Args[2])
	check(err)

	fmt.Println(vigenere_encrypt(key, data))

}
