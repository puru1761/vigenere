#!/bin/bash

echo "[*] Building vigenere-encrypt..."
go build -o binaries/vigenere-encrypt vigenere-encrypt/vigenere-encrypt.go
echo "[*] Building vigenere-decrypt..."
go build -o binaries/vigenere-decrypt vigenere-decrypt/vigenere-decrypt.go
echo "[*] Building vigenere-keylength..."
go build -o binaries/vigenere-keylength vigenere-keylength/vigenere-keylength.go 
echo "[*] Building vigenere-cryptanalyze..."
go build -o binaries/vigenere-cryptanalyze vigenere-cryptanalyze/vigenere-cryptanalyze.go
echo "[!] Build Finished!"
echo "To find and run the programs: "
echo "$ cd binaries"
