package main

import ("fmt"
	"os"
	"io/ioutil"
	"regexp"
	"strings"
	"bytes"
)

func show_usage() {

	fmt.Println("\033[91m[X]\033[0m Vigenere Keylength Guessing Tool \033[91m[X]\033[0m")
	fmt.Println("A tool for guessing the keylength of files encrypted with the\nvigenere cipher based on the index of co-incidence method")
	fmt.Printf("\n")
	fmt.Println("usage: vigenere-keylength <FILE>\n")
}

func check(e error) {
	if e != nil {
		show_usage()
		panic(e)
	}
}

func strip (data string) string {

	re, err := regexp.Compile("[^A-Za-z]+")
	check(err)

	processed_data := re.ReplaceAllString(data, "")

	return processed_data
}

func get_IC(data string) float64 {

	var counts [26]int
	var sum int
	var ic float64
	len_data := len(data)

	sum = 0
	ic = 0.0
	data = strings.ToLower(data)
	for i:=0; i < 26; i++ {
		counts[i] = 0
	}

	for i:=0; i<len_data; i++ {
		temp := int(data[i]) - 97
		counts[temp] = counts[temp] + 1
	}

	for i := 0; i < 26; i++ {
		sum = sum + (counts[i]*(counts[i]-1))
	}

	ic = float64(sum)/float64((len_data*(len_data-1)))

	return ic


}

func guess_key_len(data string) int {

	var avg_ic_array []float64
	var buf bytes.Buffer
	var ic_sum float64
	var ic_avg float64
	var sum float64
	var num int
	var key_len int

	processed_data := strip(data)
	data_len := len(processed_data)

	for i := 2; i < 65; i++ {
		ic_sum = 0.0
		for j:=0; j < i; j++ {
			for k:=j; k < data_len; k+=i {
				buf.WriteString(string(processed_data[k]))
			}
			ic_sum += get_IC(buf.String())
			buf.Reset()
		}
		ic_avg = ic_sum/float64(i)
		avg_ic_array = append(avg_ic_array, ic_avg)
	}

	sum = 0.0
	for p := range avg_ic_array {
		sum += avg_ic_array[p]
	}

	threshold := 0.06

	num = 0
	key_len = 0
	for i:=0; i < len(avg_ic_array); i++ {
		if avg_ic_array[i] >= threshold {
			//fmt.Printf("\033[93m[+]\033[0m Found possible Key Length: %d\n", (i+2))
			if key_len == 0 {
				key_len = i+2
			}
			num++
		}
	}

	//fmt.Printf("\033[91m[*]\033[0m Total Guesses for Key Length: %d\n", num)

	return key_len
}

func main() {

	if len(os.Args) < 2 {
		show_usage()
		os.Exit(1)
	}

	data, err := ioutil.ReadFile(os.Args[1])
	check(err)

	//fmt.Printf("\n")
	//fmt.Printf("\033[94m[*]\033[0m Using the Index of Coincidence method to obtain the key length for '%s'\n", os.Args[1])
	key_length := guess_key_len(string(data))
	//fmt.Printf("\n")
	fmt.Printf("%d\n", key_length)


}
