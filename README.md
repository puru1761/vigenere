# Vigenere Cipher - Cryptography and Cryptanalysis tools

This is a short study on the Vigenere cipher. This project focuses on the use of the vigenere cipher as well as techniques for exploiting the weaknesses that this cipher contains.

This project contains tools for:

* Performing vigenere encryption (vigenere-encrypt)
* Performing vigenere decryption (vigenere-decrypt)
* Guessing the Key Length of a Vigenere cipher text (vigenere-keylength)
* Completely cryptanalyzing a Vigenere cipher text with a known/unknown Key Length (vigenere-cryptanalyze)

## Build and Run Instructions

### Building with the Build script
This project contains a build script which can be used to build all of the diffeirent tools which are included here. The build script is a simple shell script which can be run directly. To run the build script and access the binaries: 

```
$ ./build.sh
$ cd binaries
```
In short:

* Build the project
* A new directory will be created named 'binaries'. All the compiled binaries will be in this folder.

### Building Each project individually
This section contains the build instructions for each of the different tools contained within this project.

### vigenere-encrypt

To build vigenere-encrypt:
```
$ cd vigenere-encrypt
$ go build
```

To run:
```
$ ./vigenere-encrypt <key> <plaintext file>
```
### vigenere-decrypt

To build vigenere-decrypt:
```
$ cd vigenere-decrypt
$ go build
```

To run:
```
$ ./vigenere-decrypt <key> <ciphertext file>
```
### vigenere-keylength

To build vigenere-keylength:
```
$ cd vigenere-keylength
$ go build
```

To run:
```
$ ./vigenere-keylength <ciphertext file>
```
### vigenere-cryptanalyze

To build vigenere-cryptanalyze:
```
$ cd vigenere-cryptanalyze
$ go build
```

To run:

* With a known keylength
```
$ ./vigenere-cryptanalyze <ciphertext file> <known keylength>
```

* Without a known Keylength
```
$ ./vigenere-cryptanalyze <ciphertext file>
```

## Author

* Purushottam Kulkarni (pkulkar6@jhu.edu)
