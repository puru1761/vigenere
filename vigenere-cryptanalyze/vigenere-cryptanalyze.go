package main

import ("fmt"
	"os"
	"io/ioutil"
	"regexp"
	"strings"
	"bytes"
	"strconv"
	"math"
)

func show_usage() {

	fmt.Println("\033[91m[X]\033[0m Vigenere Cryptanalysis Tool \033[91m[X]\033[0m")
	fmt.Println("A tool for cryptanalyzing files encrypted with the vigenere cipher\nbased on the index of co-incidence and chi-squared methods")
	fmt.Printf("\n")
	fmt.Println("\033[95m[!]\033[0m If Keylength is known")
	fmt.Println("usage: vigenere-cryptanalyze <FILE> <KEYLENGTH>\n")
	fmt.Println("\033[95m[!]\033[0m If Keylength is unknown")
	fmt.Println("usage: vigenere-cryptanalyze <FILE>\n")
}

func check(e error) {
	if e != nil {
		show_usage()
		panic(e)
	}
}

func strip (data string) string {

	re, err := regexp.Compile("[^A-Za-z]+")
	check(err)

	processed_data := re.ReplaceAllString(data, "")

	return processed_data
}

func get_IC(data string) float64 {

	var counts [26]int
	var sum int
	var ic float64
	len_data := len(data)

	sum = 0
	ic = 0.0
	data = strings.ToLower(data)
	for i:=0; i < 26; i++ {
		counts[i] = 0
	}

	for i:=0; i<len_data; i++ {
		temp := int(data[i]) - 97
		counts[temp] = counts[temp] + 1
	}

	for i := 0; i < 26; i++ {
		sum = sum + (counts[i]*(counts[i]-1))
	}

	ic = float64(sum)/float64((len_data*(len_data-1)))

	return ic


}

func calc_chi_sq(data string) float64 {

	var counts [26]int
	var sum1 float64

	expected_values := []float64{0.08167,0.01492,0.02782,0.04253,0.12702,0.02228,0.02015,0.06094,0.06966,0.00153,0.00772,0.04025,0.02406,0.06749,0.07507,0.01929,0.00095,0.05987,0.06327,0.09056,0.02758,0.00978,0.02360,0.00150,0.01974,0.00074}
	for i:=0; i<26; i++ {
		counts[i] = 0
	}

	data = strip(data)
	data = strings.ToLower(data)

	len_data := len(data)
	for i:=0; i<len_data; i++ {
		temp := int(data[i]) - 97
		counts[temp] = counts[temp] + 1
	}

	sum1 = 0.0
	for i:=0; i<26; i++ {
		sum1 = sum1 + math.Pow((float64(counts[i]) - expected_values[i]*float64(len_data)), 2)/(float64(len_data)*expected_values[i])
	}

	return sum1

}

func guess_key_len(data string) int {

	var avg_ic_array []float64
	var buf bytes.Buffer
	var ic_sum float64
	var ic_avg float64
	var sum float64

	processed_data := strip(data)
	data_len := len(processed_data)

	for i := 2; i < 65; i++ {
		ic_sum = 0.0
		for j:=0; j < i; j++ {
			for k:=j; k < data_len; k+=i {
				buf.WriteString(string(processed_data[k]))
			}
			ic_sum += get_IC(buf.String())
			buf.Reset()
		}
		ic_avg = ic_sum/float64(i)
		avg_ic_array = append(avg_ic_array, ic_avg)
	}

	sum = 0.0
	for p := range avg_ic_array {
		sum += avg_ic_array[p]
	}
	//mean := sum/float64(len(avg_ic_array))
	threshold := 0.06

	for i:=0; i < len(avg_ic_array); i++ {
		if avg_ic_array[i] >= threshold {
			fmt.Printf("\033[93m[+]\033[0m Found possible Key Length: %d\n", (i+2))
			crack_vigenere(data, (i+2))
		}
	}

	return 0
}

func min_ind(a [26]float64) int {

	var min_index int
	var min_value float64

	min_value = 0.0
	for i:=0;i<len(a);i++ {
		if i == 0 {
			min_value = a[i]
			min_index = i
		} else if a[i] < min_value {
			min_value = a[i]
			min_index = i
		}
	}

	return min_index
}

func ceasar_decrypt(data string, key int) string{

	var buf bytes.Buffer

	data = strip(data)
	data = strings.ToUpper(data)
	var ct = make([]int, len(data))
	var pt = make([]int, len(data))

	for i:=0; i<len(data); i++ {
		ct[i] = int(data[i])
		pt[i] = ct[i] - key

		if pt[i] < 65 {
			pt[i] = pt[i] + 26
		}
		buf.WriteString(string(pt[i]))
	}

	return buf.String()
}

func crack_vigenere(data string, key_len int) {

	var chi_sq_arr [26]float64
	var buf bytes.Buffer
	var key bytes.Buffer

	data = strip(data)

	len_data := len(data)
	for i:=0; i<key_len; i++ {
		for j:=i; j<len_data; j+=key_len {
			buf.WriteString(string(data[j]))
		}
		current_ceasar := buf.String()
		buf.Reset()
		for k:=0; k<26; k++ {
			dec_ceasar := ceasar_decrypt(current_ceasar, k)
			chi_sq_arr[k] = calc_chi_sq(dec_ceasar)
		}
		mi := min_ind(chi_sq_arr)
		key.WriteString(string(mi+65))
	}
	fmt.Printf("%s\n", key.String())
}

func main() {

	if len(os.Args) < 2 {
		show_usage()
		os.Exit(1)
	}

	data, err := ioutil.ReadFile(os.Args[1])
	check(err)

	if len(os.Args) == 2 {

		fmt.Printf("\n")
		//fmt.Printf("\033[94m[*]\033[0m Performing Cryptanalysis of '%s' with unknown keylength\n", os.Args[1])
		guess_key_len(string(data))
		os.Exit(1)

	} else if len(os.Args)==3 {

		key_length, err := strconv.Atoi(os.Args[2])
		check(err)

		//fmt.Printf("\033[94m[*]\033[0m Performing Cryptanalysis of '%s' with known keylength %d\n", os.Args[1], key_length)
		crack_vigenere(string(data), key_length)

	}


}
